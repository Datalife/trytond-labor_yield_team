datalife_labor_yield_team
=========================

The labor_yield_team module of the Tryton application platform.

[![Build Status](https://drone.io/gitlab.com/datalifeit/trytond-labor_yield_team/status.png)](https://drone.io/gitlab.com/datalifeit/trytond-labor_yield_team/latest)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
