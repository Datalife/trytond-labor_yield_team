# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import labor_yield
from . import timesheet


def register():
    Pool.register(
        labor_yield.YieldAllocation,
        timesheet.Work,
        module='labor_yield_team', type_='model')
