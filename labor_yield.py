# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Equal, Eval, Not, In

__all__ = ['YieldAllocation']


class YieldAllocation(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocation'

    _RQ_STATE = Equal(
        Eval('_parent_work', {}).get('yield_record_granularity', 'employee'),
        'team')

    team = fields.Many2One('company.employee.team', 'Team',
        select=True, ondelete='RESTRICT',
        states={
            'required': _RQ_STATE,
            'readonly': Not(_RQ_STATE),
            'invisible': Not(In(
                Eval('_parent_work', {}).get('yield_record_granularity',
                    'employee'), ['employee', 'team']))},
        depends=['work'])
