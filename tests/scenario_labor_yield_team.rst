=========================
Labor Yield Team Scenario
=========================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.company_employee_team.tests.tools import \
    ...     create_team, get_team


Install labor_yield_team::

    >>> config = activate_modules('labor_yield_team')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


create_team::

    >>> _ = create_team()
    >>> team = get_team()


Create work::

    >>> Work = Model.get('timesheet.work')
    >>> work = Work(name='Work 1')
    >>> work.yield_available = True
    >>> work.manual_yield_record = True
    >>> work.yield_record_granularity = 'team'
    >>> work.save()
    >>> config._context['work'] = work.id


Create allocation::

    >>> Alloc = Model.get('labor.yield.allocation')
    >>> alloc = Alloc()
    >>> alloc.work = work
    >>> alloc.quantity = 100
    >>> alloc.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.RequiredValidationError: The field "Team" on "Labor Yield Allocation" is required. - 
    >>> alloc.team = team
    >>> alloc.save()